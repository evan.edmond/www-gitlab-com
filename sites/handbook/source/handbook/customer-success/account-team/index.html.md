---
layout: handbook-page-toc
title: "Account Team"
description: "The account team works together to drive value, success, and growth for a customer"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- - -

The account team works together to drive value, success, and growth for a customer.

## Enterprise

An Enterprise account team is comprised of the following people:

- [Strategic Account Leader (SAL)](/job-families/sales/strategic-account-leader/)
- [Solutions Architect (SA)](/job-families/sales/solutions-architect/)
- [Technical Account Manager (TAM)](/job-families/sales/technical-account-manager/) for [qualifying accounts](/handbook/customer-success/tam/services/#tam-alignment)

This is commonly referred to as the "SAL-SA-TAM" team (sometimes also written as "SALSATAM").

## Commercial/Mid-Market

A Mid-Market account team is comprised of the following people:

- [Account Executive (AE)](/job-families/sales/account-executive/)
- [Technical Account Manager (TAM)](/job-families/sales/technical-account-manager/) for [qualifying accounts](/handbook/customer-success/tam/services/#tam-alignment)

In Mid-Market, Solutions Architects are pooled so they are not aligned with specific account teams.

## Account Team Meeting

The account team should meet regularly to review their accounts. Some common topics to discuss during an account team meeting include:

- The status of accounts in general, particularly those with a [health score other than Green](/handbook/customer-success/tam/health-score-triage/#health-assessment-guidelines)
- Open opportunities for growth, upgrade, and/or renewal of existing customers
  - Coordinated customer growth/expansion strategies
- Open opportunities for new business
- The status of onboarding for recently added customers

These meetings should be scheduled on a recurring basis, for the team to review their accounts and make sure they are aligned on their efforts. A weekly cadence is recommended, but if there is a small number of accounts or the accounts are low-touch, a biweekly cadence may be more appropriate.

TAMs should leverage their Gainsight TAM Portfolio dashboard to open discussion and dialogue. See [Using Gainsight in Account Team Meetings](https://youtu.be/gT_pz9PoHHg) video. 

Account teams may choose to include the [Sales Development Representative (SDR)](/job-families/marketing/sales-development-representative/) in their account meetings. The SDR primarily focuses on generating new business so they are not necessarily involved in existing customer accounts, but it may be beneficial to keep everyone informed across the entire account development lifecycle.

## SA-TAM Account Handoff and Overlap

### Account Handoff from SA to TAM

When a pre-sales account will be [TAM-qualifying](/handbook/customer-success/tam/services/#tam-alignment) and it is time for the TAM to be aligned, a formal account review and handoff from the SA to the TAM should be conducted. In addition, the SAL should also be involved in this process for discussions related to account strategy, Command Plan, etc. but can be just the SA and TAM when reviewing the more technical aspects of the customer's GitLab deployment and usage.

#### General Account Review

To start the account handoff and ensure the TAM is aware of the account details, a conversation between the SAL, SA, and TAM should take place to go over the general information about the account, to include:

- Account name and industry sector
- Key people (champion, economic buyer, decision maker, etc.)
- Command Plan and reason for purchasing GitLab
    - Defined business outcomes
    - Pain points
    - Metrics

The TAM should be able to gather enough information from this meeting to start assembling the [Success Plan](/handbook/customer-success/tam/success-plans/) for the customer, and prepare for the [onboarding process](/handbook/customer-success/tam/onboarding/).

#### Technical Handoff

As part of the pre-sales process, there are times where a PoV is conducted. If successful, The SA and TAM should review the outcome of the PoV. This should encompass the current state of the customer's GitLab usage including their progress towards a production GitLab instance, their intended GitLab architecture, the features and stages of GitLab that have been evaluated and are being initially implemented, and the questions and concerns that the customer raised during the evaluation. If a collaboration project was created by the SA during the pre-sales process, this should be shared with the TAM and reviewed as well.

#### TAM Introduction

Once the internal account review and handoff process has been conducted, the TAM should be [introduced to the customer and start the TAM engagement](/handbook/customer-success/tam/engagement/). The SA may remain involved through the initial engagement to help provide any additional details that may not have been addressed during handoff.

The SA owns all pre-sales technical relationships and activities. The SA leads technical conversations prior to the sale. TAM involvement prior to the sale models the expectations for customer relationships after the sale. TAM involvement should supplement, not displace, SA pre-sales ownership of the account.

#### **SA/TAM Overlap**

TAM engagement prior to the sale should occur in the following situations:

- At the POV completion & presentation or when appropriate in deep conversations on technology if no POV will occur
- When a shared Slack channel is created for the customer
- A shared customer issue tracking project has been created that will affect the account long-term
- As requested by the SA if the TAM has a specific subject matter expertise relevant to the conversation

The TAM owns the account post-sale. SAs are to remain involved in the following situations:

- Development of expansion plans with the SAL and TAM
- A new POV or product evaluation begins for a single team or an enterprise-wide upgrade.
- Any product license expansions that require overviews, education and competitive insights prior to close
- Any professional services are being discussed and an SOW may require drafting
- If a TAM is over-committed or unable to support a customer request, the SA may be requested to assist
- Support of GitLab days or other on-site evangelism of GitLab at customer sites
