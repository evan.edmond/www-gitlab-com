---
layout: handbook-page-toc
title: "Services Usage Data"
description: "GitLab's Service Usage Data policies"
---

#### Services Usage Data
GitLab collects information in order to help us understand how you use our products and services, so that we can improve and build better products and services. For information on what is collected and your options, please see below: 
 
#### SaaS GitLab.com software
GitLab collects information about how you use the features and functionality of our SaaS service, such as the number of projects, pipelines, issues, MRs, etc. We do not collect any information on the contents of your projects. We automatically log information about how you interact with the application, such as the date and time of visit, and the feature and functionality you have clicked on or used.

#### Self-managed GitLab software
GitLab collects information about usage from each Self-managed GitLab instance (Community Edition and Enterprise Edition) through Usage Ping. Usage Ping sends a payload containing data such as total number of projects and pipelines, as well as license information and hostname to GitLab. Only aggregates of usage data is sent to GitLab, we do not collect any information about what your projects contain. You can view the exact payload of the usage ping in the administration panel in GitLab. To opt-out of Usage Ping, follow the instructions [in our Usage Ping Guide](https://docs.gitlab.com/ee/development/usage_ping/index.html#disable-usage-ping).



