---
layout: handbook-page-toc
title: Monitor APM Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Monitor APM Single-Engineer Group

Monitor APM is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).
Our aim is to integrate monitoring and observability into our DevOps Platform in order to provide a convenient and cost effective solution that allows our customers to monitor the state of their applications, understand how changes they make can impact their applications performance characteristics and give them the tools to resolve issues that arise. This effort will be [SaaS first](https://about.gitlab.com/direction/#saas-first) and we will iterate by leveraging open source agents for auto-instrumentation.

### Monitoring and Observability Strategy
GitLab users can currently monitor their services and application by leveraging GitLab to install Prometheus to a GitLab managed cluster. Similarly, users can also install the ELK stack to do log aggregation and management. The advantage of using GitLab with these popular tools is users can collaborate on monitoring in the same application they use for building and deploying their services and applications.

What we've learned since that makes this particular strategy challenging are the following:

1. **Not working by default** - GitLab has to first manage the cluster, get users to install additional applications, setup Prometheus exporters, before being able to see a chart. Compared this to a vendor that has an agent that auto instruments an application, the high bar is a barrier for adoption.
1. **Mostly self-service** - Users are responsible for managing, scaling, and operating their Prometheus fleet, and ELK stack in order to use GitLab metrics and logging. Not having to manage and pay for the associated infrastructure and people are some main reasons organization outsource these tasks to vendors. When an organization chooses to manage monitoring on their own, many are perfectly happy just using the open source tools on their own, without GitLab as an additional layer that does not provide additional value currently.
1. **Wrong market** - We targeted SMBs to use our tools as a cheaper solution relative to other vendors. The problem is the total cost of ownership was not necessarily lower. Furthermore, since GitLab's solution was based on having GitLab manage the customer's Kubernetes cluster, and there wasn't necessarily a significantly large overlap between SMB customers and those that used Kubernetes, it meant our solutions was constrained to a smaller target audience.

We are intentionally shifting our strategy to account for what we learned:

1. Start with a [SaaS first](https://about.gitlab.com/direction/#saas-first). 
1. Leverage open-source agents for auto-instrumentation. Potentially leverage the GitLab Kubernetes Agent to setup exporters.
1. Lean into having an integrated monitoring/observability tool all within GitLab.

### Vacancy

We’re currently hiring for this role and looking for someone that understands the market, the opportunities and the complexities to help design, and develop our entry into the APM market with an integrated monitoring/observerability tool.  The Senior Fullstack Engineer: APM (Incubation Engineering) will focus on developing new customer facing SaaS monitoring and observability services and rolling out these services out to initial users. 

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/0_2EOavg_XQ" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

You can apply on our [careers page](https://about.gitlab.com/jobs/careers/).
